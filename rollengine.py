#!/usr/bin/env python3

import sys, rollwindow, math
from PyQt5 import QtWidgets

app = QtWidgets.QApplication(sys.argv)
window = QtWidgets.QMainWindow()
ui = rollwindow.Ui_MainWindow()
ui.setupUi(window)

def getTextBoxValue(TextBoxObj):
    if TextBoxObj.text() == '':
        return 0
    else:
        try:
            return int(TextBoxObj.text())
        except ValueError:
            msgbox = QtWidgets.QMessageBox()
            msgbox.setIcon(QtWidgets.QMessageBox.Warning)
            msgbox.setText('Some value is not an integer; assumed zero')
            msgbox.setWindowTitle('Warning')
            msgbox.exec_()
            return 0

def calcAttack():
    roll = 11
    plrSkill = getTextBoxValue(ui.attack_plrSkillTextBox)
    reqSkill = getTextBoxValue(ui.attack_reqSkillTextBox)
    plrDistance = getTextBoxValue(ui.attack_plrDistanceTextBox)
    wepDistance = getTextBoxValue(ui.attack_wepDistanceTextBox)
    buffs = getTextBoxValue(ui.attack_buffsTextBox)
    roll += buffs

    if plrSkill < reqSkill:
        roll += math.ceil((reqSkill - plrSkill) / 5)
    else:
        roll -= math.floor((plrSkill - reqSkill) / 10)

    if plrDistance > wepDistance / 2:
        roll += 1
    if plrDistance > wepDistance:
        roll += math.ceil((plrDistance - wepDistance) / 10)
    if plrDistance <= 5:
        roll -= 4

    if roll <= 1:
        return 'Autosuccess (%s)' % roll
    elif roll > 20:
        return 'Autofail (%s)' % roll
    return str(roll)

def addPlayerButton_clicked(self):
    print('clicked')

def calcButton_clicked(self):
    if ui.attack_radioButton.isChecked():
        ui.rollLabel.setText(calcAttack())

ui.addPlayerButton.clicked.connect(addPlayerButton_clicked)
ui.calcButton.clicked.connect(calcButton_clicked)

window.show()
sys.exit(app.exec_())
